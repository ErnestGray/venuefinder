import Foundation
import UIKit
import PromiseKit

class SearchResult {
    var id: String! = nil
    var name: String! = "Venue Name Unavailable"
    var distance: String! = "Unavailable"
    var typeIcon: UIImage! = nil
    var categories: Array<[String: AnyObject]>! = nil
    
    
    func urlForSearchResultIcon() -> URL? {
        var primaryCategory: [String: AnyObject] = [:]
        
        for category in categories {
            if let primary = category["primary"] as? Bool, primary {
                primaryCategory = category
            }
        }
       
        var urlString = ""
        let iconSize = "88"
        
        if let icon = primaryCategory["icon"] as? [String: AnyObject] {
            if let prefix = icon["prefix"] as? String, let suffix = icon["suffix"] as? String {
                urlString = prefix + iconSize + suffix
            }
        }
        
        return URL(string: urlString)
    }
    
    
    static func fromDict(dict: [String: AnyObject]) -> SearchResult? {
        let searchResult = SearchResult()
   
        if let id = dict["id"] as? String {
           searchResult.id = id
        }
        
        if let name = dict["name"] as? String {
            searchResult.name = name
        }
        
        if let categories = dict["categories"] as? Array<[String: AnyObject]> {
            searchResult.categories = categories
        }
        
        if let location = dict["location"] as? [String: AnyObject] {
            if let distance = location["distance"] as? Double {
                let formattedDistance = String(format: "%.1f", distance)
                searchResult.distance = "\(formattedDistance)m"
            }
            
        }
        
        return searchResult
    }
    
    func getSearchResultIcon() -> Promise<UIImage> {
        return Promise { fullfill, reject in
            let imageError = NSError(domain: "Promise Kit", code: 0, userInfo: [NSLocalizedDescriptionKey: "Image Not Found"])
            let url = urlForSearchResultIcon()
            if let url = url {
                URLSession.shared.dataTask(with: url) { data, response, error in
                    if let data = data {
                        guard let image = UIImage(data: data) else { return }
                        fullfill(image)
                    } else if let error = error {
                        reject(error)
                    }
                }.resume()
            } else {
                reject(imageError)
            }
        }.catch(execute: { (error) in
            return
        })
    }
}



