import Foundation
import UIKit
import MapKit
import PromiseKit

enum VenueImageTypes: String {
    case venueImage = "venueImage"
    case venueIcon = "venueIcon"
}

class Venue {
    fileprivate var id: String! = nil
    var name: String! = "Venue Title Unavailable"
    var contact: String! = "Venue Phone Number Unavailable"
    var address: String! = "Venue Title Unavailable"
    var rating: Double! = 0.0
    var ratingColor: String! = "000000"
    var type: String! = "Venue Type Unavailable"
    var description: String! = "Venue Description Unavailable"
    var status: String! = "Venue Status Unavailable"
    var categories: Array<[String: AnyObject]>! = nil
    var bestPhoto: [String: AnyObject]! = nil

    func urlForVenueImage() -> URL? {
        var urlString = ""
        let imageSize = "300x300"
        
        if let best = bestPhoto {
            if let prefix = best["prefix"] as? String, let suffix = best["suffix"] as? String {
                urlString = prefix + imageSize + suffix
            }
        }
        
        return URL(string: urlString)
    }
    
    func urlForSearchVenueIcon() -> URL? {
        var primaryCategory: [String: AnyObject] = [:]
        
        for category in categories {
            if let primary = category["primary"] as? Bool, primary {
                primaryCategory = category
            }
        }
        
        var urlString = ""
        let iconSize = "88"
        
        if let icon = primaryCategory["icon"] as? [String: AnyObject] {
            if let prefix = icon["prefix"] as? String, let suffix = icon["suffix"] as? String {
                urlString = prefix + iconSize + suffix
            }
        }
        
        return URL(string: urlString)
    }
    
    func venueType() -> String {
        var types:[String] = []
        for category in categories {
            if let shortName = category["shortName"] as? String {
                types.append(shortName)
            }
        }
        return types.joined(separator: ", ")
    }
    
    static func fromDict(dict: [String: AnyObject]) -> Venue? {
        let venue = Venue()

        if let name = dict["name"] as? String {
            venue.name = name
        }
        
        if let contact = dict["contact"] as? [String: AnyObject] {
            venue.contact = contact["formattedPhone"] as? String ?? ""
        }
        
        if let location = dict["location"] as? [String: AnyObject] {
            venue.address = (location["formattedAddress"] as? [String])?.joined(separator: " \n") ?? ""
        }
        
        if let categories = dict["categories"] as? Array<[String: AnyObject]> {
            venue.categories = categories
        }
        
        if let rating = dict["rating"] as? Double {
            venue.rating = rating
        }
        if let ratingColor = dict["ratingColor"] as? String {
            venue.ratingColor = ratingColor
        }
        
        if let bestPhoto = dict["bestPhoto"] as? [String: AnyObject] {
            venue.bestPhoto = bestPhoto
        }
        
        if let hours = dict["hours"] as? [String: AnyObject] {
            venue.status = hours["status"] as? String ?? ""
        }
        
        if let description = dict["description"] as? String {
            venue.description = description
        }
        
        return venue
    }
    
    func getVenueImageForType(type: VenueImageTypes ) -> Promise<UIImage> {
        let url:URL? = type == .venueIcon ? urlForSearchVenueIcon() : urlForVenueImage()
        return Promise(resolvers: { (fullfill, reject) in
            let imageError = NSError(domain: "Promise Kit", code: 0, userInfo: [NSLocalizedDescriptionKey: "Image Error"])
            if let url = url {
                URLSession.shared.dataTask(with: url) { data, response, error in
                    if let data = data {
                        guard let image = UIImage(data: data) else { return }
                        fullfill(image)
                    } else if let error = error {
                        reject(error)
                    }
                }.resume()
            } else {
                reject(imageError)
            }
        }).catch(execute: { (error) in
            return
        })
    }
}


