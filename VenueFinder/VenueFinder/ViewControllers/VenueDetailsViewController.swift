import UIKit

class VenueDetailsViewController: UIViewController {
   
    @IBOutlet weak var venueImage: UIImageView!
    @IBOutlet weak var venueIcon: UIImageView!
    @IBOutlet weak var venueTitle: UILabel!
    @IBOutlet weak var venueType: UILabel!
    @IBOutlet weak var venueAddress: UILabel!
    @IBOutlet weak var venuePhoneNumber: UILabel!
    @IBOutlet weak var venueRating: UILabel!
    @IBOutlet weak var venueRatingView: UIView!
    @IBOutlet weak var venueStatus: UILabel!
    @IBOutlet weak var venueDescription: UILabel!
    
    let cornerRadius:CGFloat = 10.0
    var venue: Venue?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let venue = venue else {
            return
        }
        
        venue.getVenueImageForType(type: .venueImage).then(on: DispatchQueue.main) { icon -> UIImage in
            return icon
            }.then { icon -> Void in
                venue.getVenueImageForType(type: .venueIcon).then(on: DispatchQueue.main) { venueIcon -> Void in
                    self.venueImage.image = icon
                    self.venueIcon.image = venueIcon
                }
            }
        
        self.venueTitle.text = venue.name
        self.venueType.text = venue.venueType()
        self.venueAddress.text = venue.address
        self.venuePhoneNumber.text = venue.contact
        self.venueRating.text = String(format: "%.1f", venue.rating)
        self.venueRatingView.backgroundColor = UIColor(hexString: venue.ratingColor as String!)
        self.venueStatus.text = venue.status
        self.venueDescription.text = venue.description
        
        self.venueRatingView.layer.cornerRadius = cornerRadius
        self.venueIcon.layer.cornerRadius = cornerRadius
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}


