import UIKit
import PromiseKit

class SearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let fourSquareService = FourSquareService.sharedInstance
    var searchResults: [SearchResult] = []
    var venue: Venue?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell", for: indexPath) as! SearchResultTableViewCell
        let searchResult = searchResults[indexPath.row]
        
        cell.titleLabel.text = searchResult.name
        cell.distanceLabel.text = searchResult.distance
        searchResult.getSearchResultIcon().then { icon -> Void in
            cell.iconImage.image = icon
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow!
        let searchResult = searchResults[indexPath.row]
        fourSquareService.getVenueById(searchResult.id).then { venue -> Void in
            self.venue = venue
            self.performSegue(withIdentifier: "venueDetailsSegue", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "venueDetailsSegue" {
            if let destination = segue.destination as? VenueDetailsViewController {
                destination.venue = venue
            }
        }
    }
        
}

