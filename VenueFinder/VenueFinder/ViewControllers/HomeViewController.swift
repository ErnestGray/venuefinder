import UIKit
import CoreLocation

class HomeViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var searchAnotherLocationButton: UIButton!
    @IBOutlet weak var searchNearMeButton: UIButton!
    @IBOutlet weak var locationTextField: UITextField!
    
    let fourSquareService = FourSquareService.sharedInstance
    let locationManager = CLLocationManager()
    let cornerRadius:CGFloat = 5
    var searchResults: [SearchResult] = []
    var locationString: String = ""
    var enteredLocation: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchAnotherLocationButton.layer.cornerRadius = cornerRadius
        searchNearMeButton.layer.cornerRadius = cornerRadius
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func searchNearMeTapped(_ sender: UIButton) {
        fourSquareService.getSearchResultsForLocation(locationCoordinates: locationString).then { results -> Void in
            self.searchResults = results
            self.performSegue(withIdentifier: "searchResultListSegue", sender: self)
        }
    }
    
    @IBAction func searchLocationTapped(_ sender: UIButton) {
        if (locationTextField.text!).isEmpty {
            showEmptyTextFieldAlert()
        } else {
            fourSquareService.getSearchResultsForLocation(enteredLocation: locationTextField.text!).then { results -> Void in
                self.searchResults = results
                self.performSegue(withIdentifier: "searchResultListSegue", sender: self)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchResultListSegue" {
            if let destination = segue.destination as? SearchResultsViewController {
                destination.searchResults = self.searchResults
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first  {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            locationString = "\(latitude),\(longitude)"
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledAlert()
        }
    }
    
    func showLocationDisabledAlert() {
        let alertController = UIAlertController(title: "Location Access Disabled", message: "We need your location to search nearby venues", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Go To Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showEmptyTextFieldAlert() {
        let alertController = UIAlertController(title: "Search Location is Empty", message: "Please enter a location", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
