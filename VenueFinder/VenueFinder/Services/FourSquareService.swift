import Foundation
import PromiseKit
import MapKit

class FourSquareService: NSObject {
    
    static let sharedInstance = FourSquareService()
    private override init() {}
    
    func getSearchResultsForLocation(locationCoordinates: String? = nil, enteredLocation: String? = nil ) -> Promise<[SearchResult]> {
        return Promise(resolvers: { (fullfill, reject) in
            let url = searchResultQuery(locationCoordinates: locationCoordinates, enteredLocation: enteredLocation)
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                let searchParseError = NSError(domain: "Promise Kit", code: 0, userInfo: [NSLocalizedDescriptionKey: "Search Result Parse Error"])
                guard error == nil else {
                    reject(error!)
                    return
                }
                guard let data = data else {
                    reject(searchParseError)
                    return
                }
                
                do {
                    guard let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                        reject(searchParseError)
                        return
                    }
                    
                    guard let response = result["response"] as? [String : Any] else {
                        reject(searchParseError)
                        return
                    }
                    
                    guard let venues = response["venues"] as? [AnyObject] else {
                        reject(searchParseError)
                        return
                    }
                    
                    fullfill(venues.flatMap { SearchResult.fromDict(dict: $0 as? [String : AnyObject] ?? [:]) })
                } catch  {
                    reject(error)
                    return
                }
            }
            task.resume()
        })
    }
    
    
    
    
    func getVenueById(_ id: String) -> Promise<Venue> {
        return Promise(resolvers: { (fulfill, reject) in
            let url = venueQuery(id: id)
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                let venueParseError = NSError(domain: "Promise Kit", code: 0, userInfo: [NSLocalizedDescriptionKey: "Venue Parse Error"])
                guard error == nil else {
                    reject(error!)
                    return
                }
                guard let data = data else {
                    reject(venueParseError)
                    return
                }
                
                do {
                    guard let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                        reject(venueParseError)
                        return
                    }
                    
                    guard let response = result["response"] as? [String : Any] else {
                        reject(venueParseError)
                        return
                    }
                    
                    guard let venue = response["venue"] as? [String : AnyObject] else {
                        reject(venueParseError)
                        return
                    }
                    
                    fulfill(Venue.fromDict(dict: venue)!)
                } catch {
                    reject(error)
                    return
                }
            }
            task.resume()
        })
    }
    
    func searchResultQuery(locationCoordinates: String? = nil, enteredLocation: String? = nil ) -> URL {
        let coordinates = locationCoordinates ?? ""
        let location = enteredLocation ?? ""
        var components = URLComponents()
        
        components.scheme = "https"
        components.host = "api.foursquare.com"
        components.path = "/v2/venues/search"
        
        let queryItemLocation = coordinates.isEmpty ? URLQueryItem(name: "near", value: location) : URLQueryItem(name: "ll", value: coordinates)
        let queryItemClientId = URLQueryItem(name: "client_id", value: "WHS5JV2WJDYUS4XTAVPXYTP2TKBMWSYB33VTV55W4BOGWJBP")
        let queryItemSecretId = URLQueryItem(name: "client_secret", value: "J4FCCAX3KRYJW5R4ET0OMBJJPI1FTVDGIYVHXLDRMAQLNTBF")
        let queryItemVersion = URLQueryItem(name: "v", value: "20180107")
        components.queryItems = [queryItemLocation, queryItemClientId, queryItemSecretId, queryItemVersion]
        
        return components.url!
    }
    
    func venueQuery(id: String ) -> URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.foursquare.com"
        components.path = "/v2/venues/\(id)"
        
        let queryItemClientId = URLQueryItem(name: "client_id", value: "WHS5JV2WJDYUS4XTAVPXYTP2TKBMWSYB33VTV55W4BOGWJBP")
        let queryItemSecretId = URLQueryItem(name: "client_secret", value: "J4FCCAX3KRYJW5R4ET0OMBJJPI1FTVDGIYVHXLDRMAQLNTBF")
        let queryItemVersion = URLQueryItem(name: "v", value: "20180107")
        components.queryItems = [queryItemClientId, queryItemSecretId, queryItemVersion]
        
        return components.url!
    }
}
